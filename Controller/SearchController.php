<?php


class SearchController
{
    public static function IsPostData()
    {
        if(isset($_POST) && isset($_POST['zip_code'])){
            $zip_result = ZipCodes::selectByZipCode($_POST['zip_code']);
            if(isset($zip_result) && count($zip_result) == 0){
                $data = ApiService::getDataFromApi($_POST['country'],$_POST['zip_code']);
                if(isset($data) && count($data) > 0 && !isset($data['message'])){
                    self::insertDataFromApi($data);
                    return $data;
                }
                else{
                    return $data;
                }
            }
            else{
                $data = self::getDataFromDB($_POST['zip_code'],$_POST['country']);
                return $data;
            }
        }
    }

    public static function insertDataFromApi($data){
        $zip_id = ZipCodes::insertZipCode($data[0]['zip_code']);
        foreach($data as $value){
            if(isset($zip_id)){
                CountryCodes::insertCountryCode($value,$zip_id);
            }
        }
    }

    public static function getDataFromDB($zip_code,$country)
    {
        $zip_data = ZipCodes::selectByZipCode($zip_code);
        $zip_id = isset($zip_data) && isset($zip_data[0]) ? $zip_data[0]['id'] : '';
        $country_data = [];
        if(isset($zip_id)){
            $country_data = CountryCodes::selectByZipCode($zip_id,$country);
        }
        if(count($country_data) == 0){
            $country_data['message'] =  "There is no data with this code";
        }
        return $country_data;
    }

}