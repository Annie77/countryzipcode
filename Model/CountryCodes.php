<?php


class CountryCodes extends DataBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function createTable($table)
    {
        switch ($table) {
            case 'country_codes':
                $sql = "CREATE TABLE country_codes (
                id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                zip_id INT (9) NOT NULL,
                country VARCHAR (50) NOT NULL,
                abbreviation VARCHAR (4) NOT NULL,
                name VARCHAR(50) NOT NULL,
                longitude VARCHAR (20) NOT NULL,
                latitude VARCHAR (20) NOT NULL,
                created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                updated  TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW()
                )";
                break;
        }
        self::Connection()->query($sql);
    }

    public static function insertCountryCode($data, $zip_id)
    {
        $country = $data['country'];
        $name = $data['name'];
        $abbreviation = $data['abbreviation'];
        $longitude = $data['longitude'];
        $latitude = $data['latitude'];
        $sql = "INSERT INTO country_codes(zip_id, country, abbreviation, name, longitude, latitude) VALUES ('$zip_id', '$country', '$abbreviation', '$name', '$longitude', '$latitude')";
        self::Connection()->query($sql);
    }

    public static function selectByZipCode($zip_code,$country)
    {
        $sql = "SELECT * 
                FROM country_codes
                where zip_id='$zip_code' AND abbreviation='$country'";
        $result = self::Connection()->query($sql)->fetch_all(true);
        return $result;
    }
}