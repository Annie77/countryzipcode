<?php


class DataBase
{
    public function __construct()
    {
        self::InitConnection();
    }

    public static function Connection()
    {
        $db = new mysqli("localhost", "root", "", "country_code");
        return $db;
    }

    public static function InitConnection()
    {
        self::initTables();
        if (!self::Connection()) self::Connection()->close();
    }

    public static function initTables()
    {
        $tables = [
            ['country_codes', 'CountryCodes'],
            ['zip_codes', 'ZipCodes']
        ];
        foreach ($tables as $table) {
            $row = mysqli_query(self::Connection(), 'select 1 from `' . $table[0] . '` LIMIT 1');
            if(!$row) self::call_child_method($table[0],$table[1]);
        }
    }

    public static function call_child_method($table, $className)
    {
        switch ($className) {
            case 'CountryCodes' :
                CountryCodes::createTable($table);
                break;
            case 'ZipCodes' :
                ZipCodes::createTable($table);
                break;
        }
    }
}