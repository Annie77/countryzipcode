<?php


class ZipCodes extends DataBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function createTable($table)
    {
        switch ($table) {
            case 'zip_codes':
                $sql = "CREATE TABLE zip_codes (
                id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                zip_code VARCHAR (10) NOT NULL,
                created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                updated  TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW()
                )";
                break;
        }
        self::Connection()->query($sql);
    }

    public static function insertZipCode($zip_code)
    {
        $sql = "INSERT INTO zip_codes(zip_code) VALUES ('$zip_code')";
        if (self::Connection()->query($sql)) {
            $result = self::selectByZipCode($zip_code);
            $zip_id = isset($result) && isset($result[0]) ? $result[0]['id'] : '';
            return $zip_id;
        }
    }

    public static function selectByZipCode($zip_code)
    {
        $sql = "SELECT * 
                FROM zip_codes
                where zip_code='$zip_code'";
        $result = self::Connection()->query($sql)->fetch_all(true);
        return $result;
    }
}