<?php


class ApiService
{
    public static function getDataFromApi($country,$zip_code)
    {
        $data = [];
        $client = new GuzzleHttp\Client();
        $url = 'http://www.zippopotam.us/' . $country . '/' . $zip_code;
        try{
            $response = $client->get($url);
            $response_data = (string) $response->getBody();
            $data = json_decode($response_data);
            return self::getData($data);
        }
        catch (Exception $e){
            $data['message'] = "There is no data with this code";
            return $data;
        }
    }

    public static function getData($data)
    {
        $array_data = [];
        $data = (array) $data;
        if(isset($data['places']) && count($data['places']) > 0){
            foreach($data['places'] as $key =>  $value){
                $value = (array) $value;
                $array_data[$key]['zip_code'] = $data['post code'];
                $array_data[$key]['country'] = $data['country'];
                $array_data[$key]['abbreviation'] = $data['country abbreviation'];
                $array_data[$key]['name'] = $value['place name'];
                $array_data[$key]['longitude'] = $value['longitude'];
                $array_data[$key]['latitude'] = $value['latitude'];
            }
        }
        return $array_data;
    }
}