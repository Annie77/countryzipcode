<?php


class CountryName
{
    public static function getCountryNames()
    {
        $data = [
            ['name' => 'United States', 'code' => 'US'],
            ['name' => 'Germany',       'code' => 'DE'],
            ['name' => 'Great Britain', 'code' => 'GB'],
            ['name' => 'France',        'code' => 'FR'],
            ['name' => 'Canada',        'code' => 'CA'],
            ['name' => 'Denmark',       'code' => 'DK'],
            ['name' => 'Italy',         'code' => 'IT'],
            ['name' => 'Japan',         'code' => 'JP'],
            ['name' => 'Russia',        'code' => 'RU'],
            ['name' => 'Sweden',        'code' => 'SE'],
        ];
        return $data;
    }

    public static function getNameByCode($code)
    {
        $name = '';
        $data = self::getCountryNames();
        foreach($data as $value){
            if($value['code'] === $code){
                $name = $value['name'];
            }
        }
        return $name;
    }

}