<?php
require_once('resource/header.php');
$data = SearchController::IsPostData();
$countries = CountryName::getCountryNames();

?>
<h3>Search Country Code</h3>

<form action="" method="post">
    <div class=" row">
        <div class="col-4">
            <lable>Select Country</lable>
            <select class="form-control" name="country">
                <?php if(isset($countries) && count($countries) > 0){
                    foreach($countries as $country){ ?>
                        <option></option>
                        <option <?php echo isset($_POST) && isset($_POST['country']) && $_POST['country'] === $country['code'] ? "selected" : "" ?>
                            value="<?php echo $country['code']?>"><?php echo $country['name']?></option>
                  <?php  }
                }?>
            </select>
        </div>
        <div class="col-4">
            <lable for="zip_code">Zip Code</lable>
            <input class="form-control" name="zip_code" value="<?php echo isset($_POST) && isset($_POST['zip_code']) ? $_POST['zip_code'] : ''?>"/>
        </div>
        <div class="col-4 pt-4">
            <button id="search_country" class="btn btn-primary" type="submit">Search</button>
        </div>

    </div>

</form>
<?php if(isset($data) && count($data) > 0 && !isset($data['message'])){?>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Place Name</th>
        <th scope="col">Latitude</th>
        <th scope="col">Longitude</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($data as $value){?>
    <tr>
        <td><?php echo $value['name']?></td>
        <td><?php echo $value['latitude']?></td>
        <td><?php echo $value['longitude']?></td>
    </tr>
    <?php }?>
    </tbody>
</table>
<?php }
else{ ?>
    <h3 class="text-danger"><?php echo isset($data['message']) ? $data['message'] : '';?></h3>
<?php }?>
<?php
require_once('resource/footer.php');
?>

