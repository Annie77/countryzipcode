<?php
require_once ('vendor/autoload.php');
require_once ('Model/DataBase.php');
require_once ('Model/ZipCodes.php');
require_once ('Model/CountryCodes.php');
require_once ('Service/ApiService.php');
require_once ('Service/CountryName.php');
require_once ('Controller/SearchController.php');
$db = new DataBase();