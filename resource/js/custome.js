$(document).ready(function(){
    $('#search_country').on('click',function(e){
        removeError();
        let country = $('select[name="country"]').val();
        let countryElem = $('select[name="country"]').parent();
        let zip_code = $('input[name="zip_code"]').val();
        let zip_codeElem = $('input[name="zip_code"]').parent();
        let error = false;
        if(country.length === 0){
            error = true;
            errorMessage(countryElem);
        }
        if(zip_code.length === 0){
            error = true;
            errorMessage(zip_codeElem);
        }
        if(error){
            e.preventDefault();
        }
    });

    $('select[name="country"]').on('change',function(e){
       $(this).parent().find('p.error_message').remove();
    });

    $('input[name="zip_code"]').on('input',function(e){
       $(this).parent().find('p.error_message').remove();
    });

    function errorMessage(elem){
        elem.append(`<p class="error_message text-danger small">Required Field!</p>`)
    }

    function removeError()
    {
        $('p.error_message').remove();
    }
});